#!/usr/bin/env python3
# Copyright (C) 2021 Marcin Włodarczak
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from collections import defaultdict
import os
from glob import glob
import re
import sys

import pandas as pd

import tgt


def nspeakers(intr):

    label = intr.text
    if re.search(r'[wb]ss', label):
        return 0
    elif re.search(r'[wb]so', label):
        return len(intr.text.split(':')[1].split(','))
    else:
        return 1


def main(root_dir):

    bigrams = {}

    for path in glob(os.path.join(root_dir, '*')):

        if not os.path.isdir(path):
            continue

        lang = re.search(r'(SWE|EST|ENG)$', path).group(0)
        bigrams[lang] = defaultdict(int)

        print(f'Processing {lang}', file=sys.stderr)

        for tg_path in glob(os.path.join(path, '*.TextGrid')):
            tg = tgt.read_textgrid(tg_path)
            chrono = tgt.util.chronogram(tg.tiers)

            for bigram in zip(chrono[:-1], chrono[1:]):
                label = '_'.join(str(nspeakers(i)) for i in bigram)
                bigrams[lang][label] += 1

    return pd.DataFrame(bigrams)


if __name__ == '__main__':

    bigrams = main(sys.argv[1])
    bigrams['total'] = bigrams.sum(axis=1)

    bigrams_perc = 100 * bigrams / bigrams.sum()
    bigrams_perc.to_csv('bigrams.csv', index=False, na_rep=0)

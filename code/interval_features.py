#!/usr/bin/env python3
# Copyright (C) 2021 Marcin Włodarczak
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from glob import glob
import csv
import os
import re
import string
import sys

import tgt


class Transition:

    def __init__(self, intervals):

        self.spkr_first = intervals[0].text
        self.spkr_last = intervals[-1].text
        self.trans_type = 'WST' if self.spkr_first == self.spkr_last else 'BST'
        self.dur = intervals[-1].start_time - intervals[0].end_time

        self.intervals = []
        for intr in intervals:
            intr.speakers = self._interval_speakers(intr)
            self.intervals.append(intr)

    def __len__(self):
        return len(self.intervals) - 2

    def __getitem__(self, key):
        return self.intervals[key]

    def __iter__(self):
        return iter(self.intervals)

    @staticmethod
    def _interval_speakers(intr):
        '''Return labels for all speakers in a (chronogram) interval.'''

        if re.match(r'[bw]so', intr.text):
            return sorted(intr.text.split(':')[1].split(','))

        elif re.match(r'[bw]ss', intr.text):
            return []

        else:
            return [intr.text]

    @property
    def nspeakers(self):
        '''Return the number of speaker speaking during the transition.'''
        return len(self.unique_speakers)

    @property
    def label_num(self):
        '''Return a numerical label for a transition.'''
        return '_'.join(str(len(i.speakers)) for i in self)

    def label_full(self, order='firstlast'):
        '''Return a full label for a transition.

        The original speaker labels are replaced with Latin capital
        letters. In within-speaker transitions, speakers are labelled
        in the order in which they take the floor. In between-speaker
        transitions, two ways of ordering speakers is supported:

        - 'firstlast': the first speaker is labelled 'A', the last
          speaker is labelled B, the other speakers are labelled in
          the order in which they take the floor.

        - 'chrono': the first speaker is labelled 'A', the other
          speakers are labelled in the order in which they take the
          floor.
        '''

        label = []
        if order == 'chrono':
            trans = {}
        elif order == 'firstlast':
            if self.trans_type == 'BST':
                trans = {self.spkr_first: 'A',
                         self.spkr_last: 'B'}
            else:
                trans = {self.spkr_first: 'A'}
        else:
            raise ValueError(f'Invalid order argument: {order}')

        for intr in self:
            intr_speakers = []
            for spkr in intr.speakers:
                if spkr not in trans:
                    trans[spkr] = string.ascii_uppercase[len(trans)]
                intr_speakers.append(trans[spkr])
            label.append(':'.join(sorted(intr_speakers)))

        return '_'.join(label).replace('__', '_X_')

    @property
    def unique_speakers(self):
        '''Return a set of all speakers speaking during the intervening
        intervals of a transition.
        '''
        return set(spkr for intr in self[1: -1] for spkr in intr.speakers)

    @property
    def other_parties(self):
        '''Return True if the intervning intervals involve spekers other than
        first or last, otherwise return False.
        '''
        return self.unique_speakers - {self.spkr_first, self.spkr_last}

    @property
    def solo(self):
        '''Return all single-speaker intervening intervals.'''
        return [i for i in self[1: -1]
                if not re.match(r'[bw]s[so]', i.text)]

    @property
    def silences(self):
        '''Return all silent intervening intervals.'''
        return [i for i in self if re.match(r'[bw]ss', i.text)]

    @property
    def overlaps(self):
        '''Return all intervening intervals including overlappings speech.'''
        return [i for i in self if re.match(r'[bw]so', i.text)]

    @property
    def dur_solo(self):
        '''Return the total duration of all single-speaker intervening
        intervals.'''
        return sum(i.duration() for i in self.solo)

    @property
    def perc_dur_solo(self):
        '''Return the duration of all single-speaker intervening intervals as
        a percentage of the duration of the entire transition.
        '''
        try:
            return 100 * self.dur_solo / self.dur
        except ZeroDivisionError:
            return None

    @property
    def dur_silence(self):
        '''Return the total duration of all silent intervening intervals.'''
        return sum(i.duration() for i in self.silences)

    @property
    def perc_dur_silence(self):
        '''Return the druation of all silent intervening intervals as a
        percentage of the duration of the entire transition.
        '''
        try:
            return 100 * self.dur_silence / self.dur
        except ZeroDivisionError:
            return None

    @property
    def dur_overlap(self):
        '''Return the total duration of all overlapping intervening
        intervals.
        '''
        return sum(i.duration() for i in self.overlaps)

    @property
    def perc_dur_overlap(self):
        '''Return the duratio of all overlapping intervening intervals as a
        percentage of the duration of the entire transition.'''
        try:
            return 100 * self.dur_overlap / self.dur
        except ZeroDivisionError:
            return None

    @property
    def solo_1SpAny(self):
        '''Return the index and label of the next single-speaker interval of
        any duration.
        '''
        return [(i, intr.text) for i, intr in enumerate(self[1:])
                if not re.match(r'[bw]s[so]', intr.text)][0]

    @property
    def trans_type_1SpAny(self):
        '''Return the type of the transitition from the first speaker to the
        next single-speaker interval of any duration.
        '''
        next_1sp = self.solo_1SpAny[1]
        if self.spkr_first == next_1sp:
            return 'WST'
        else:
            return 'BST'

    @property
    def nintr_1SpAny(self):
        '''Return the number of intervening intervals involved in the
        transitition from the first speaker to the next single-speaker
        interval of any duration.
        '''
        return self.solo_1SpAny[0]


def find_transitions(chrono, min_solo_dur=1):
    '''Given a chronogram, find all transitions between single-speaker
    intervals longer or equal to min_solo_dur.
    '''

    transitions = []
    trans = []
    for intr in chrono:
        # Check whether it is a single-speaker interval.
        is_solo = re.match(r'[bw]s[so]', intr.text) is None
        # If the interval involves silence or overlapping speech or
        # whether it is shorter than min_solo_dur, and if we are in
        # the middle of a transition, store it.
        if not is_solo or intr.duration() < min_solo_dur:
            if trans:
                trans.append(intr)
        # If it is a single-speaker interval longer than min_solo_dur,
        # store it and if the resulting transition is longer than 1
        # (i.e. if the current interval is not transition-initial),
        # start a new transition.
        else:
            trans.append(intr)
            if len(trans) > 1:
                transitions.append(Transition(trans))
                trans = [intr]
    return transitions


def process_corpus(data_dir, min_solo_dur):
    '''Process all TextGrid files in data_dir.'''

    # Get langauge label.
    lang = data_dir.split(os.sep)[-1].lower()
    transitions = []

    for tg_path in glob(os.path.join(data_dir, '*.TextGrid')):
        tg = tgt.read_textgrid(tg_path)
        # Create the chronogram.
        chrono = tgt.util.chronogram(tg.tiers)
        # Identify transitions.
        transitions += find_transitions(chrono, min_solo_dur)

    # Calculate and store transition features.
    trans_features = []
    for tr in transitions:
        trans_features.append({
            'lang': lang,
            'spkr_first': tr.spkr_first,
            'spkr_last': tr.spkr_last,
            'trans_type': tr.trans_type,
            'nintr': len(tr),
            'label_num': tr.label_num,
            'label_full': tr.label_full(order='firstlast'),
            'label_full_chrono': tr.label_full(order='chrono'),
            'nspeakers': tr.nspeakers,
            'includes_other_parties': bool(tr.other_parties),
            'n_other_parties': len(tr.other_parties),
            'includes_overlap': bool(tr.overlaps),
            'includes_silence': bool(tr.silences),
            'dur': tr.dur,
            'dur_solo': tr.dur_solo,
            'perc_dur_solo': tr.perc_dur_solo,
            'dur_overlap': tr.dur_overlap,
            'perc_dur_overlap': tr.perc_dur_overlap,
            'dur_silence': tr.dur_silence,
            'perc_dur_silence': tr.perc_dur_silence,
            'dur_spkr_first': sum(i.duration() for i in tr.intervals[1:-1]
                                  if tr.spkr_first in i.speakers),
            'dur_spkr_last': sum(i.duration() for i in tr.intervals[1:-1]
                                 if tr.spkr_last in i.speakers),
            'dur_spkr_other': sum(i.duration() for i in tr.intervals[1:-1]
                                  if set(i.speakers) & tr.other_parties),
            'dur_1sp_spkr_first': sum(i.duration() for i in tr.intervals[1:-1]
                                      if i.text == tr.spkr_first),
            'dur_1sp_spkr_last': sum(i.duration() for i in tr.intervals[1:-1]
                                     if i.text == tr.spkr_last),
            'dur_1sp_spkr_other': sum(i.duration() for i in tr.intervals[1:-1]
                                      if i.text in tr.other_parties),
            'trans_type_1SpAny': tr.trans_type_1SpAny,
            'nintr_1SpAny': tr.nintr_1SpAny
        })

    return trans_features


def main(root_dir, min_solo_dur):

    # Find all subdirectories of root_dir
    data_dirs = [path for path in glob(os.path.join(root_dir, '*'))
                 if os.path.isdir(path)]

    # Process each subdirectory and store the resulting transition
    # features.
    features = []
    for path in data_dirs:
        features += process_corpus(path, min_solo_dur)

    # Save the result in a CSV file.
    outpath = 'intervals.csv'
    with open(outpath, 'w') as fout:
        header = list(features[0].keys())
        csv_writer = csv.DictWriter(fout, fieldnames=header)
        csv_writer.writeheader()
        csv_writer.writerows(features)


if __name__ == '__main__':

    main(sys.argv[1], float(sys.argv[2]))

#!/usr/bin/env python3

from collections import defaultdict
from glob import glob
import os
import re
import sys

import tgt


def parse_time(time):
    start, end = time.split('_')
    return float(start) / 1000, float(end) / 1000


def main(cha_path, outdir):

    regex = re.compile(r'(?:\*(\w+):)?\s+(.*?)(?:{}([\d_]+){})?$'.format(
        chr(0x15), chr(0x15)))

    fname = os.path.basename(cha_path).replace('.cha', '')
    print('Processing {}...'.format(fname), file=sys.stderr)

    with open(cha_path) as cha_file:

        spurts = defaultdict(list)
        prev_spkr, prev_spurt, prev_time = None, None, None

        for line in cha_file:
            line = line.rstrip()

            if line.startswith('%') or line.startswith('@'):
                skip_lines = True
            elif line.startswith('*'):
                skip_lines = False

            if skip_lines:
                continue

            spkr, spurt, time = regex.search(line).groups()
            if prev_spurt is not None:
                spkr = spkr or prev_spkr
                spurt = prev_spurt + ' ' + spurt
            if time is not None:
                start, end = parse_time(time)
                # If two overlaps by the same speaker overlap, merge
                # them.
                if spurts[spkr] and start < spurts[spkr][-1].end_time:
                    spurts[spkr][-1].end_time = end
                    spurts[spkr][-1].text += ' ' + spurt
                else:
                    spurts[spkr].append(tgt.Interval(start, end, spurt))
                prev_spkr, prev_spurt, prev_time = None, None, None
            else:
                prev_spkr, prev_spurt, prev_time = spkr, spurt, time

    if all(not len(v) for v in spurts):
        print('  No annotations available!', file=sys.stderr)
        return

    tg = tgt.TextGrid()
    for spkr in ['A', 'B']:
        tier = tgt.IntervalTier(name=spkr, objects=spurts[spkr])
        tg.add_tier(tier)

    tgt.write_to_file(tg, os.path.join(outdir, 'csv', fname + '.csv'),
                      format='table', separator='\t')

    chrono = tgt.util.chronogram(tg.tiers)
    chrono.name = 'chronogram'
    tg.add_tier(chrono)
    tgt.write_to_file(tg, os.path.join(outdir, 'textgrids', fname + '.TextGrid'))


if __name__ == '__main__':

    ROOT_DIR = '/home/marcin/Data/CallHome/English/docs/eng/'
    OUTDIR = '../data/callhome/'

    for cha_path in glob(os.path.join(ROOT_DIR, '*.cha')):
        main(cha_path, OUTDIR)
